<h1 align="center">Huddle landing page with alternating feature blocks</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://huddle-landing-page-with-alternating-feature-blocks-sigma-inky.vercel.app/">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/Vivek03t/huddle-landing-page-with-alternating-feature-blocks">
      Solution
    </a>
    <span> | </span>
    <a href="https://www.figma.com/file/kQsMHu1NEhPt5oV4Dcne5K/huddle-landing-page-with-alternating-feature-blocks?node-id=0%3A479&mode=dev">
      Challenge
    </a>
  </h3>
</div>

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Overview](#overview)
  - [Built With](#built-with)
- [Features](#features)
- [Acknowledgements](#acknowledgements)
- [Contact](#contact)

<!-- OVERVIEW -->

## Overview

![screenshot](design/desktop-design.jpg),

![screenshot](design/active-states.jpg),

![screenshot](design/mobile-design.jpg),

### Built With

<!-- This section should list any major frameworks that you built your project using. Here are a few examples.-->

- [HTML]
- [CSS]

## Features

<!-- List the features of your application or follow the template. Don't share the figma file here :) -->

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge. The [challenge](https://devchallenges.io/challenges/hhmesazsqgKXrTkYkt0U) was to build an application to complete the given user stories.


## Acknowledgements

<!-- This section should list any articles or add-ons/plugins that helps you to complete the project. This is optional but it will help you in the future. For exmpale -->

- [Steps to replicate a design with only HTML and CSS](https://devchallenges-blogs.web.app/how-to-replicate-design/)
- [Marked - a markdown parser](https://github.com/chjj/marked)

## Contact

- GitHub [@tivivek](https://github.com/tivivek)
